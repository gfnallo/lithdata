Lithium data
============

This python module contains various data useful for physical modeling of lithium, especially neutral lithium vapor.

As functions of temperature:

- Vapor pressure
- Viscosity
- Thermal conductivity
- Self-diffusivity

This module is most concerned with temperatures from 300 K to 1500 K.

Multiple sources from the literature are included wherever possible. All data sources are cited.
